package com.ef.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LogServiceTests {

	LogService logService;

	@Before
	public void setup() {
		logService = new LogService();
	}

	@Test
	public void canCreateArgumentService() {
		assertNotNull(logService);
	}

}
