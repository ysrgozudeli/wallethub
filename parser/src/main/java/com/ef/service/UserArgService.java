package com.ef.service;

import java.io.File;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import com.ef.helper.Helper;
import com.ef.model.UserArgument;

@Service
public class UserArgService {
	public static final Logger logger = LoggerFactory.getLogger(UserArgService.class);

	public final String DURATION_DAILY = "daily";
	public final String DURATION_HOURLY = "hourly";

	private final String DURATION = "duration";
	private final String START_DATE = "startDate";
	private final String THRESHOLD = "threshold";
	private final String LOG_FILE_PATH = "accesslog";

	public UserArgument parseUserConfiguration(ApplicationArguments arguments) {
		validateInputParams(arguments);
		return prepareUserArgs(arguments);
	}

	private void validateInputParams(ApplicationArguments userArgs) {

		if (userArgs.getSourceArgs().length == 0 || userArgs == null) {

			throw new IllegalArgumentException("No argument is given. Please feed the correct argument set. E.g. --accesslog=/path/to/file --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100 ");
		}

		validateRequiredArgument(userArgs);
	}

	private void validateRequiredArgument(ApplicationArguments userArgs) {

		if (!userArgs.containsOption(DURATION)) {
			throw new IllegalArgumentException("The --duration argument is required.");
		}

		if (!userArgs.containsOption(START_DATE)) {
			throw new IllegalArgumentException("The --startDate argument is required.");
		}

		if (!userArgs.containsOption(THRESHOLD)) {
			throw new IllegalArgumentException("The --duration argument is required.");
		}
	}

	private UserArgument prepareUserArgs(ApplicationArguments arguments) {

		UserArgument userValues = new UserArgument();

		userValues.setDuration(extractDuration(arguments));
		userValues.setThreshold(extractThreshold(arguments));
		userValues.setStartDate(extractStartDate(arguments));
		userValues.setEndDate(calculateEndDate(userValues.getDuration(), userValues.getStartDate()));
		userValues.setLogLocation(extractLogFilePath(arguments));
		return userValues;
	}

	private Date calculateEndDate(String duration, Date startDate) {

		if (DURATION_HOURLY.equals(duration)) {
			return Helper.addHour(startDate);
		} else {
			return Helper.addDay(startDate);
		}
	}

	// work with the old data in the database, if file location is not given
	private File extractLogFilePath(ApplicationArguments arguments) {

		if (arguments.containsOption(LOG_FILE_PATH)) {
			return extractFileFromInput(arguments);
		}
		return null;
	}

	private File extractFileFromInput(ApplicationArguments arguments) {
		String filePath = arguments.getOptionValues(LOG_FILE_PATH).get(0).toLowerCase();
		File file = new File(filePath);

//		if (!file.exists()) {
//			throw new IllegalArgumentException("Can't find the file specified by --accesslog: " + filePath);
//		}

		return file;
	}

	private long extractThreshold(ApplicationArguments arguments) {

		try {
			String threshold = arguments.getOptionValues(THRESHOLD).get(0).toLowerCase();
			return Long.parseLong(threshold);
		} catch (NumberFormatException ex) {
			throw new IllegalArgumentException("The 'threshold' should be a number.  ");
		}

	}

	private Date extractStartDate(ApplicationArguments arguments) {

		return Helper.parseInputDate(arguments.getOptionValues(START_DATE).get(0).toLowerCase());

	}

	private String extractDuration(ApplicationArguments arguments) {

		String duration = arguments.getOptionValues(DURATION).get(0).toLowerCase();

		if (!DURATION_DAILY.equals(duration) && !DURATION_HOURLY.equals(duration)) {
			throw new IllegalArgumentException("The duration should be one of the either 'daily' or 'hourly' ");
		}

		return duration;

	}

}
