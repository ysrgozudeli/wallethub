package com.ef.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ef.model.UserArgument;
import com.ef.repository.LogRepository;

@Service
public class LogService {
	public static final Logger logger = LoggerFactory.getLogger(LogService.class);

	@Autowired
	private LogRepository logRepo;

	UserArgument userConf;

	public List<String> parse(UserArgument userConf) throws Exception {
		this.userConf = userConf;

		// clear the old data if log file is given
		if (userConf.getLogLocation() != null) {
			// clear tables
			cleanTables();
			loadLogFile();

			// no significant performance difference. So the index creation is disabled..
			// createIndex();
		}

		return findBlockedIPs();

	}

	private void cleanTables() {
		logRepo.cleanLogTable();
		logRepo.cleanBlockedIpTable();
//		logRepo.cleanLogTableIndex();
	}

	private void loadLogFile() {
//		String path = this.userConf.getLogLocation().toString();  
//		System.out.println(path);
		logger.info("LOADING BULK DATA STARTED...");
		logRepo.bulkLoadData(userConf.getLogLocation().getAbsolutePath());
		logger.info("LOADING BULK DATA ENDED");
	}

	private void createIndex() {
		logRepo.createLogTableIndex();
	}

	private List<String> findBlockedIPs() {
		logger.info("Calculating the blocked IPs...");
		return logRepo.generateBlockedIpList(this.userConf.getStartDate(), this.userConf.getEndDate(),
				this.userConf.getThreshold());

	}

}
